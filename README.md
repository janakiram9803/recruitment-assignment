# Assignment - Restaurant Management System


# Functional Requirements
- A food menu which lists various sections (starters / main course / desserts) having items inside each.
- A price chart associated with every item on the menu.
- The customers who visit the restaurant.
- Hosts and hostesses who allot customers tables or queue them up if all tables are in use.
- Bus boys who serve water, clean tables, lay out fresh cloth, etc. 
- Managers who take initial orders, receive complaints, prepare and hand out the bills, etc.
- Servers who serve the food on the table.
- Executive chef who receives orders and expedites food preparation
- Line cooks who prepare the food.
- Order register where the customer orders are stored
- Cash register where bills are generated and payments are stored
- Feedback system where customer complaints and feedback comments are stored
- The general restaurant facilities such as tables, chairs, utensils, so on so forth.


# Technical requirements
- Typescript is the programming language and Angular is the webframework to be used to develop the solution
- Angular Material themeing must be implemented
- npm should be used as the build system for compiling and producing the final js code
- Wherever possible, decouple data from the system. e.g. the menu items could be read from a file rather than hard coding within the solution
 
Dinner In The Sky
============


Dinner In The Sky is a food ordering and restaurant management system. TastyIgniter provides a professional and reliable platform for restaurants wanting to offer online ordering to their customers.

The best place to learn Dinner In The Sky is by reading the [documentation]

## Credits
- D.V.Janakiram.